# Blender WebGPU Docker

## Project Overview
This project aims to port Blender to the web using WebGPU for graphics rendering, encapsulated within a Docker container for ease of development, reproducibility, and deployment. By leveraging WebAssembly and WebGPU, we envision bringing Blender's powerful 3D modeling, animation, and rendering capabilities to a broader audience through web browsers.

## Getting Started

### Prerequisites
- Docker
- Git

### Clone the Repository
```bash
git clone https://github.com/anthony-firn/blender-webgpu-docker.git
cd blender-webgpu-docker
```

### Building and Running with Docker Compose
Navigate to the docker directory if your Docker-related files are located there, then use Docker Compose to build and run the container.
```bash
cd docker
docker-compose up --build
```

## Project Structure
- `/docker`: Contains `Dockerfile` and `docker-compose.yml` for Docker configuration.
  - `Dockerfile`: Docker setup including the installation of Emscripten, and other dependencies.
  - `docker-compose.yml`: Configuration for orchestrating Docker containers.
- `/src`: Modified Blender source code and WebGPU adaptations.
- `/scripts`: Scripts for automating the build and compile processes.
- `/patches`: Patch files for modifications to Blender's codebase for WebGPU support.
- `/docs`: Documentation on project setup, build instructions, and contribution guidelines.

## Development Workflow
1. **Environment Setup**: Initialize the environment using Docker to ensure all dependencies are installed.
2. **Code Modification**: Adapt Blender's source code for WebGPU compatibility.
3. **Build and Compile**: Compile the modified Blender source to WebAssembly.
4. **Testing**: Conduct automated tests to verify functionality.
5. **Deployment**: Package the compiled code for web deployment.

## Contributing
Contributions are welcome! Please:
1. Fork the repository.
2. Create a feature branch: `git checkout -b feature/AmazingFeature`.
3. Commit changes: `git commit -m 'Add some AmazingFeature'`.
4. Push to the branch: `git push origin feature/AmazingFeature`.
5. Submit a pull request.

## License
This project is released under the [MIT License](LICENSE.md).

## Acknowledgments
- [Blender Foundation](https://www.blender.org/) for Blender.
- [Emscripten](https://emscripten.org/) for the WebAssembly compilation toolchain.
- [WebGPU Headers](https://github.com/juj/wasm_webgpu) for enabling WebGPU support in C programs.
- [BlazorHybridWebGPUTest](https://github.com/MichaelPeter/BlazorHybridWebGPUTest) for insights into WebGPU with web technologies.

## Additional Resources
- [Blender Developer Documentation](https://wiki.blender.org/)
- [WebGPU Specification](https://gpuweb.github.io/gpuweb/)
- [Docker Documentation](https://docs.docker.com/)